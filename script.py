import ipaddress

# Define the network
network = ipaddress.ip_network('2001:1998:64a:20:3:0:6:0/112')

# Initialize a counter
count = 0

# Iterate over the network's hosts
for address in network.hosts():
    print(address)
    count += 1
    if count == 16:
        break
