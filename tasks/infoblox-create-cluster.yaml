  vars_files:
    - vars/stage/infoblox-creds.yaml
    - vars/stage/infoblox-vars.yml
  tasks: 
    # The playbook starts by fetching IPv6 networks from an Infoblox server. 
    # It retrieves IPv6 networks within a specified network container and stores the result in the result variable. 
    - name: Fetch IPv6 /112 networks from {{networkcontainer}} network container
      uri:
        url: "{{ infoblox_url }}/wapi/{{ wapi_version }}/ipv6network?network_container={{ networkcontainer }}&_return_fields={{ return_fields }}"
        method: GET
        user: "{{ user }}"
        password: "{{ password }}"
        validate_certs: no
        return_content: yes
      register: result
      failed_when: false
    
    # After fetching the IPv6 networks, the playbook checks if the fetch was successful. 
    # If there is a failure, it handles the error by displaying a message and gracefully ending the play.
    - name: Check if the fetch was successful
      block:
        - name: Handle Infoblox fetch failure
          debug:
            msg: "Failed to fetch data from Infoblox. Status code: {{ result.status }}"
          when: result.status != 200

        - name: Exit gracefully if fetch failed
          meta: end_play
          when: result.status != 200


    ##############################################################################################
    #                 PLAY WHEN CAAS CLUSTER IS FOUND - STARTS HERE                              # 
    ##############################################################################################    

    # The playbook then checks if a specific extensible attribute value (CaaS Cluster name) exists in any network. 
    # It sets the caas_cluster_networks variable to hold networks where the CaaS Cluster value is found.
    - name: Check if extensible attribute value exists in any network
      set_fact:
        caas_cluster_networks: >-
          {{
            result.json | json_query('[?extattrs.' + caas_cluster_extattr + '.value == `' + caas_cluster_value + '`]')
          }}
    
    # If the CaaS Cluster value is found in any networks, the playbook displays a message listing the networks.
    - name: Display message if the extensible attribute value already exists
      debug:
        msg: >-
          The CaaS Cluster Value '{{ caas_cluster_value }}' is already assigned to the following networks:
          {{ caas_cluster_networks | map(attribute='network') | join(', ') }}
      when: caas_cluster_networks | length > 0

    # If the CaaS Cluster value is found, the playbook saves the network address of the first network where the CaaS Cluster value is found into the assigned_network variable.
    - name: Save network address as a variable
      set_fact:
        assigned_network: "{{ caas_cluster_networks | map(attribute='network') | first }}"
      when: caas_cluster_networks | length > 0

    # This play ensures the .tfvar exist
    - name: Ensure the .tfvars file exists
      file:
        path: ./vars/output_file.tfvars
        state: touch

    - name: Replace or append the fact in the Terraform .tfvars file
      lineinfile:
        path: ./vars/output_file.tfvars
        regexp: '^network\s*='
        line: 'network = "{{ assigned_network }}"'
        create: yes
      when: assigned_network is defined

    # This simply outputs the saved network ( Not necessary but for confirmation that the network was saved)
    - debug:
        msg: "{{ assigned_network }}"
      when: caas_cluster_networks | length > 0
    
    # If the CaaS Cluster value is found, the playbook exits gracefully to end the play.
    - name: Exit gracefully if the extensible attribute value already exists
      meta: end_play
      when: caas_cluster_networks | length > 0


    ##############################################################################################
    #                 PLAY WHEN CAAS CLUSTER ISN'T FOUND STARTS HERE                             # 
    ##############################################################################################

    # If the CaaS Cluster value is not found in any networks, the playbook prints a message indicating this absence.
    - name: Print message if CaaS_Cluster were not found
      debug:
        msg: "No networks found with CaaS_Cluster == '{{ caas_cluster_value }}'"
      when: caas_cluster_networks | length == 0
    
    # This task Set_fact for IPv6 /112 networks.
    - name: Check if any networks with IPv6 /112 already exist
      set_fact:
        existing_112_networks: "{{ caas_cluster_networks | selectattr('network', 'search', '/{{ subnet }}$') | list }}"
      when: caas_cluster_networks | length == 0

    # The playbook requests the next available IPv6 /112 network from Infoblox if no existing /112 networks are found.
    - name: Request IPv6 /112 from Infoblox if no existing /112 networks
      uri:
        url: "{{ infoblox_url }}/wapi/{{ wapi_version }}/ipv6network"
        method: POST
        user: "{{ user }}"
        password: "{{ password }}"
        validate_certs: no
        headers:
          Content-Type: "{{ content_type }}"
        body_format: json
        body: '{"network": "func:nextavailablenetwork:{{ networkcontainer }},{{ networkview }},{{ subnet }}"}'
        status_code: [200, 201]
      register: new_network_response
      when: existing_112_networks | length == 0

    # Saves the next available network requested from previous play
    - set_fact:
        ipv6_network: "{{ new_network_response.json.split(':')[1] | regex_replace('%3A', ':') | regex_replace('%2F', '/') | regex_replace('%3A', ':')  | regex_replace('/default$', '') }}"
      when: new_network_response is defined and new_network_response.json is defined

    # If the CaaS Cluster value does not exist, the playbook creates an extensible attribute definition for it.
    - name: Create extensible attribute definition if it doesn't exist
      uri:
        url: "{{ infoblox_url }}/wapi/{{ wapi_version }}/extensibleattributedef"
        method: POST
        user: "{{ user }}"
        password: "{{ password }}"
        validate_certs: no
        headers:
          Content-Type: "application/json"
        body_format: json
        body:
          name: "{{ caas_cluster_extattr }}"
          type: "STRING"
      register: result
      failed_when: false
      changed_when: result.status != 409
    
    # This task retrieves the reference ID value of the next available network created
    - name: Retrieving the reference ID value of the network created
      uri:
        url: "{{ infoblox_url }}/wapi/{{ wapi_version }}/ipv6network?network_container={{ networkcontainer }}&_return_fields={{ return_fields }}"
        method: GET
        user: "{{ user }}"
        password: "{{ password }}"
        validate_certs: no
      register: result
    
     # This task saves the retrieved reference ID value of the next available network created
    - name: Saving the next available network reference ID value
      set_fact:
        ref_value: "{{ result.json[-1]._ref }}"

    # After fetching the next available network and creating the extensible attribute, 
    # the playbook assigns the CaaS Cluster value to the next available network.
    - name: Assigning {{ caas_cluster_value }} to the created network {{ ipv6_network }}
      uri:
        url: "{{ infoblox_url }}/wapi/{{ wapi_version }}/{{ ref_value }}"
        method: PUT
        user: "{{ user }}"
        password: "{{ password }}"
        validate_certs: no
        headers:
          Content-Type: application/json
        body_format: json
        body:
          extattrs:
            CaaS_Cluster:
              value: "{{ caas_cluster_value }}"
      register: response
    
    # It then displays a message confirming the assignment of the CaaS Cluster to the network.
    - debug:
        msg: "{{ caas_cluster_value }} assigned to {{ ipv6_network }}"
      when: ipv6_network is defined

    # This task saves the network address - to be imported in a different playbook
    - name: Saving next available network address as a variable
      set_fact:
        assigned_network: "{{ ipv6_network }}"
      when: new_network_response is defined and new_network_response.json is defined

    - name: Ensure the .tfvars file exists
      file:
        path: ./vars/output_file.tfvars
        state: touch

    - name: Replace or append the fact in the Terraform .tfvars file
      lineinfile:
        path: ./vars/output_file.tfvars
        regexp: '^network\s*='
        line: 'network = "{{ assigned_network }}"'
        create: yes
      when: new_network_response is defined and new_network_response.json is defined

    # - name: Echo the fact into a file using the copy module foe new network
    #   copy:
    #     content: "network: {{ assigned_network }}"
    #     dest: ./vars/output_file.tfvars
    #   when: new_network_response is defined and new_network_response.json is defined

    # The playbook then saves the network address of the assigned network into the assigned_network variable.
    - debug:
        msg: "{{ assigned_network }}"
      when: new_network_response is defined and new_network_response.json is defined
    
    # Finally, the playbook displays a message indicating that the CaaS Cluster is already assigned to a network.
    - debug:
        msg: "{{ caas_cluster_value }} is already assigned to network"
      when: ipv6_network is not defined


    ##############################################################################################
    #                      INCLUDE / IMPORT TASJ FOR NEXR PLAYBPOOK                              # 
    ##############################################################################################
